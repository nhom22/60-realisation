//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Warehouse.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ChiTietPhieuKham
    {
        public int MaChiTietPhieuKham { get; set; }
        public int MaPhieuKham { get; set; }
        public Nullable<int> MaThuoc { get; set; }
        public Nullable<byte> SoLuong { get; set; }
        public Nullable<int> ThanhTien { get; set; }
    
        public virtual PhieuKham PhieuKham { get; set; }
        public virtual Thuoc Thuoc { get; set; }
    }
}
