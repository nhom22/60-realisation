﻿using System;

namespace Warehouse.Models
{
    public class Format
    {
        public static string FormatCurrencyVND(int? price)
        {
            return price != null ? price.Value.ToString("#,##0").Replace(',', '.') + " đ" : "";
        }
        public static string FormatDateTime(DateTime dateTime)
        {
            return dateTime.ToString("HH:mm dd/MM/yyyy");
        }
    }
}
