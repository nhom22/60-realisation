﻿using System;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Warehouse.Models
{
    public class Functions
    {
        private Functions()
        {

        }
        /// <summary>
        /// Get All Errors In Page
        /// </summary>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        public static string GetAllErrorsPage(ModelStateDictionary ModelState)
        {
            StringBuilder strb = new StringBuilder("<ul>");
            foreach (string key in ModelState.Keys)
            {
                ModelState[key].Errors.ToList().ForEach(m => strb.Append("<li>" + m.ErrorMessage + "</li>"));
            }
            strb.Append("</ul>");
            return strb.ToString();
        }

        /// <summary>
        /// Get All Errors In Page
        /// </summary>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        public static string GetAllErrorsPageNotFormat(ModelStateDictionary ModelState)
        {
            StringBuilder strb = new StringBuilder("");
            foreach (string key in ModelState.Keys)
            {
                ModelState[key].Errors.ToList().ForEach(m => strb.Append(m.ErrorMessage + "."));
            }
            return strb.ToString();
        }

        /// <summary>
        ///  Save File Image From String Base64
        /// </summary>
        /// <param name="fileName">path save image</param>
        /// <param name="base64String">String Base64</param>
        public static void SaveFileFromBase64(string path, string base64String)
        {
            System.IO.File.WriteAllBytes(path, Convert.FromBase64String(base64String));
        }

        /// <summary>
        ///  Get Time String 
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static string GetTimePast(DateTime time)
        {
            return Math.Floor((DateTime.Now - time).TotalHours) == 0 ? Math.Ceiling((DateTime.Now - time).TotalMinutes).ToString() + " phút" : Math.Floor((DateTime.Now - time).TotalHours) > 24 ? Math.Floor((DateTime.Now - time).TotalDays).ToString() + " ngày" : Math.Floor((DateTime.Now - time).TotalHours).ToString() + " giờ";
        }

         
        /// <summary>
        /// Check Valid Email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool IsValidEmail(string email)
        {
            try
            {
                MailAddress m = new MailAddress(email);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        const string UniChars = "àáảãạâầấẩẫậăằắẳẵặèéẻẽẹêềếểễệđìíỉĩịòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵÀÁẢÃẠÂẦẤẨẪẬĂẰẮẲẴẶÈÉẺẼẸÊỀẾỂỄỆĐÌÍỈĨỊÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÙÚỦŨỤƯỪỨỬỮỰỲÝỶỸỴÂĂĐÔƠƯ";
        const string KoDauChars = "aaaaaaaaaaaaaaaaaeeeeeeeeeeediiiiiooooooooooooooooouuuuuuuuuuuyyyyyAAAAAAAAAAAAAAAAAEEEEEEEEEEEDIIIIIOOOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYYAADOOU";

        public static string UnicodeToKoDau(string s)
        {
            string retVal = string.Empty;
            for (int i = 0; i < s.Length; i++)
            {
                int pos = UniChars.IndexOf(s[i].ToString());
                if (pos >= 0)
                    retVal += KoDauChars[pos];
                else
                    retVal += s[i];
            }
            return retVal.ToLower();
        }

        public static string UnicodeToKoDauAndGach(string s)
        {
            const string strChar = "abcdefghijklmnopqrstxyzuvxw0123456789- ";
            s = UnicodeToKoDau(s.ToLower().Trim());
            string sReturn = "";
            for (int i = 0; i < s.Length; i++)
            {
                if (strChar.IndexOf(s[i]) > -1)
                {
                    if (s[i] != ' ')
                        sReturn += s[i];
                    else if (i > 0 && s[i - 1] != ' ')
                        sReturn += "-";
                }
            }
            return sReturn.Replace("--", "-").ToLower();
        }
    }
}