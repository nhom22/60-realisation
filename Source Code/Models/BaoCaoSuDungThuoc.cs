﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Warehouse.Models
{
    public class BaoCaoSuDungThuocDto
    {
        public string TenThuoc { get; set; }
        public string DonVi { get; set; }
        public int SoLuong { get; set; }
        public int SoLanDung { get; set; }
    }
}