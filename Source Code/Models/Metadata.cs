﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Warehouse.Models
{
    [MetadataType(typeof(ThuocMetadata))]
    public partial class Thuoc
    {

    }
    sealed class ThuocMetadata
    {
        [Display(Name = "Mã thuốc")]
        public int MaThuoc { get; set; }

        [Display(Name = "Tên thuốc")]
        [Required(ErrorMessage = "Bạn chưa nhập {0}")]
        public string TenThuoc { get; set; }

        [Display(Name = "Đơn vị")]
        [Required(ErrorMessage = "Bạn chưa nhập {0}")]
        public string DonVi { get; set; }

        [Display(Name = "Đơn giá")]
        public Nullable<int> DonGia { get; set; }

        [Display(Name = "Cách dùng")]
        public string CachDung { get; set; }
    }
}