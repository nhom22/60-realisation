﻿using System.Collections.Generic;
using System.Linq;
using Warehouse.Models;

namespace Warehouse.Services
{
    public class ChiTietPhieuKhamService 
    {
        #region Private property
        private QuanLyPhongMachTuEntities db = new QuanLyPhongMachTuEntities();
        #endregion

        #region Public logic method

        public List<ChiTietPhieuKham> GetByMaPhieuKham(int maPhieuKham)
        {
            return db.ChiTietPhieuKhams.Where(x => x.MaPhieuKham == maPhieuKham).ToList();
        }

        public void AddList(List<ChiTietPhieuKham> chiTietPhieuKhams)
        {
            db.ChiTietPhieuKhams.AddRange(chiTietPhieuKhams);
            db.SaveChanges();
        }

        public void DeleteByMaPhieuKham(int maPhieuKham)
        {
            List<ChiTietPhieuKham> chiTietPhieuKhams = GetByMaPhieuKham(maPhieuKham);
            db.ChiTietPhieuKhams.RemoveRange(chiTietPhieuKhams);
            db.SaveChanges();
        }

        #endregion
    }
}
