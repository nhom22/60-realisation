﻿using System.Collections.Generic;
using System.Linq;
using Warehouse.Models;

namespace Warehouse.Services
{
    public class PhieuKhamService  
    {
        #region Private property
        private QuanLyPhongMachTuEntities db = new QuanLyPhongMachTuEntities();
        #endregion

        #region Public logic method
      
        public List<PhieuKham> GetAll()
        {
            return db.PhieuKhams.Where(x => x.TrieuChung != null).OrderByDescending(x => x.NgayKham).ToList();
        }

        public PhieuKham GetPhieuKhamCuaBenhNhan(int maBenhNhan)
        {
            return db.PhieuKhams.SingleOrDefault(x => x.MaBenhNhan == maBenhNhan);
        }

        public void Add(PhieuKham phieuKham)
        {
            db.PhieuKhams.Add(phieuKham);
            db.SaveChanges();
        }

        public void Update(PhieuKham phieuKham)
        {
            db.Entry(phieuKham).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(PhieuKham phieuKham)
        {
            db.PhieuKhams.Remove(phieuKham);
            db.SaveChanges();
        }

        #endregion
    }
}
