﻿using System.Collections.Generic;
using System;
using System.Linq;
using Warehouse.Models;
namespace Warehouse.Services
{
    public class BenhNhanService  
    {
        #region Private property
        private QuanLyPhongMachTuEntities db = new QuanLyPhongMachTuEntities();
        #endregion
 
        #region Public logic method
      
        public List<BenhNhan> GetDanhSachBenhNhan()
        {
            return db.BenhNhans.Where(x => x.PhieuKham != null).ToList().Where(x => x.PhieuKham.TrieuChung != null).OrderByDescending(x => x.PhieuKham.NgayKham).ToList();
        }

        public BenhNhan GetByMaBenhNhan(int maBenhNhan)
        {
            return db.BenhNhans.Find(maBenhNhan);
        }

        public void Add(BenhNhan benhNhan)
        {
            db.BenhNhans.Add(benhNhan);
            db.SaveChanges();
        }

        public void Update(BenhNhan benhNhan)
        {
            db.Entry(benhNhan).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(BenhNhan benhNhan)
        {
            db.BenhNhans.Remove(benhNhan);
            db.SaveChanges();
        }

        public List<BenhNhan> GetDanhSachKhamBenh()
        {
            return db.BenhNhans.Where(x => x.PhieuKham != null).ToList().Where(x => x.PhieuKham.TrieuChung == null).OrderByDescending(x => x.PhieuKham.NgayKham).ToList();
        }

        public void AddRange(List<BenhNhan> benhNhans)
        {
            foreach(BenhNhan benhNhan in benhNhans)
            {
                Add(benhNhan);
            }
        }

        public bool KiemTraLuotKhamQua40Nguoi(DateTime ngayKham)
        {
            return db.BenhNhans.Where(x => x.PhieuKham != null).ToList().Where(x => x.PhieuKham.NgayKham.Date == ngayKham.Date).Count() > 40;
        }

        #endregion
    }
}
