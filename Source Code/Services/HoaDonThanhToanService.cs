﻿using System.Collections.Generic;
using System.Linq;
using Warehouse.Models;

namespace Warehouse.Services
{
    public class HoaDonThanhToanService  
    {
        #region Private property
        private QuanLyPhongMachTuEntities db = new QuanLyPhongMachTuEntities();
        #endregion

        #region Public logic method


        public List<HoaDonThanhToan> GetAll()
        {
            return db.HoaDonThanhToans.OrderByDescending(x => x.NgayLapHoaDon).ToList();
        }

        public List<HoaDonThanhToan> GetByMaPhieuKham(int maPhieuKham)
        {
            return db.HoaDonThanhToans.Where(x => x.MaPhieuKham == maPhieuKham).OrderByDescending(x => x.NgayLapHoaDon).ToList();
        }

        public void Add(HoaDonThanhToan hoaDonThanhToan)
        {
            db.HoaDonThanhToans.Add(hoaDonThanhToan);
            db.SaveChanges();
        }

        public void Update(HoaDonThanhToan hoaDonThanhToan)
        {
            db.Entry(hoaDonThanhToan).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(HoaDonThanhToan hoaDonThanhToan)
        {
            db.HoaDonThanhToans.Remove(hoaDonThanhToan);
            db.SaveChanges();
        }

        #endregion
    }
}
