﻿using System.Collections.Generic;
using System.Linq;
using Warehouse.Models;

namespace Warehouse.Services
{
    public class ThuocService  
    {
        #region Private property
        private QuanLyPhongMachTuEntities db = new QuanLyPhongMachTuEntities();
        #endregion

        #region Public logic method
      
        public List<Thuoc> GetAll()
        {
            return db.Thuocs.ToList();
        }

        public Thuoc GetByMaThuoc(int maThuoc)
        {
            return db.Thuocs.SingleOrDefault(x => x.MaThuoc == maThuoc);
        }

        public Thuoc GetByTenThuoc(string tenThuoc)
        {
            return db.Thuocs.SingleOrDefault(x => x.TenThuoc == tenThuoc);
        }

        public void Add(Thuoc thuoc)
        {
            db.Thuocs.Add(thuoc);
            db.SaveChanges();
        }

        public void Update(Thuoc thuoc)
        {
            db.Entry(thuoc).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(Thuoc thuoc)
        {
            db.Thuocs.Remove(thuoc);
            db.SaveChanges();
        }
        #endregion
    }
}
