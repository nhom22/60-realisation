CREATE Database QuanLyPhongMachTu
GO
USE QuanLyPhongMachTu
GO
GO
/****** Object:  Table [dbo].[BenhNhan]    Script Date: 7/17/2020 5:03:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BenhNhan](
	[MaBenhNhan] [int] IDENTITY(1,1) NOT NULL,
	[HoTen] [nvarchar](max) NOT NULL,
	[GioiTinh] [bit] NULL,
	[NamSinh] [int] NULL,
	[DiaChi] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.BenhNhan] PRIMARY KEY CLUSTERED 
(
	[MaBenhNhan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChiTietPhieuKham]    Script Date: 7/17/2020 5:03:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietPhieuKham](
	[MaChiTietPhieuKham] [int] IDENTITY(1,1) NOT NULL,
	[MaPhieuKham] [int] NOT NULL,
	[MaThuoc] [int] NULL,
	[SoLuong] [tinyint] NULL,
	[ThanhTien] [int] NULL,
 CONSTRAINT [PK_dbo.ChiTietPhieuKham] PRIMARY KEY CLUSTERED 
(
	[MaChiTietPhieuKham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HoaDonThanhToan]    Script Date: 7/17/2020 5:03:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HoaDonThanhToan](
	[MaPhieuKham] [int] NOT NULL,
	[TienKham] [int] NULL,
	[TienThuoc] [int] NULL,
	[NgayLapHoaDon] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.HoaDonThanhToan] PRIMARY KEY CLUSTERED 
(
	[MaPhieuKham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhieuKham]    Script Date: 7/17/2020 5:03:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhieuKham](
	[NgayKham] [datetime] NOT NULL,
	[TrieuChung] [nvarchar](max) NULL,
	[DuDoanLoaiBenh] [nvarchar](max) NULL,
	[MaBenhNhan] [int] NOT NULL,
 CONSTRAINT [PK_dbo.PhieuKham] PRIMARY KEY CLUSTERED 
(
	[MaBenhNhan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Thuoc]    Script Date: 7/17/2020 5:03:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Thuoc](
	[MaThuoc] [int] IDENTITY(1,1) NOT NULL,
	[TenThuoc] [nvarchar](max) NOT NULL,
	[DonVi] [nvarchar](max) NULL,
	[DonGia] [int] NULL,
	[CachDung] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Thuoc] PRIMARY KEY CLUSTERED 
(
	[MaThuoc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[f_ThongKeDoanhThuTheoNgay]    Script Date: 7/17/2020 5:03:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[f_ThongKeDoanhThuTheoNgay] 
(	
	 
)
RETURNS TABLE 
AS
RETURN 
(
	  SELECT A.NgayKham, COUNT(*) SoBenhNhan, SUM(B.ThanhTien) AS DoanhThu
	  FROM PhieuKham A 
	  INNER JOIN ChiTietPhieuKham B
	  ON A.MaBenhNhan = B.MaPhieuKham
	  GROUP BY A.NgayKham
)

GO
SET IDENTITY_INSERT [dbo].[BenhNhan] ON 

INSERT [dbo].[BenhNhan] ([MaBenhNhan], [HoTen], [GioiTinh], [NamSinh], [DiaChi]) VALUES (2, N'Nguyễn Ái Tuấn', 1, 1995, N'Quận 12, TPHCM')
INSERT [dbo].[BenhNhan] ([MaBenhNhan], [HoTen], [GioiTinh], [NamSinh], [DiaChi]) VALUES (3, N'ABC', 1, 1994, N'Quận 12, TPHCM')
INSERT [dbo].[BenhNhan] ([MaBenhNhan], [HoTen], [GioiTinh], [NamSinh], [DiaChi]) VALUES (4, N'test 1234', 1, 1993, N'Vũng Tàu')
INSERT [dbo].[BenhNhan] ([MaBenhNhan], [HoTen], [GioiTinh], [NamSinh], [DiaChi]) VALUES (5, N'test 1', 1, 1993, N'Quận 1')
INSERT [dbo].[BenhNhan] ([MaBenhNhan], [HoTen], [GioiTinh], [NamSinh], [DiaChi]) VALUES (6, N'test 3', 1, 1996, N'Quận 3')
INSERT [dbo].[BenhNhan] ([MaBenhNhan], [HoTen], [GioiTinh], [NamSinh], [DiaChi]) VALUES (7, N'Huynh Bao Ngoc', 0, 2002, N'Q7')
INSERT [dbo].[BenhNhan] ([MaBenhNhan], [HoTen], [GioiTinh], [NamSinh], [DiaChi]) VALUES (8, N'Huynh Bao Ngoc', 0, 2002, N'Q7')
INSERT [dbo].[BenhNhan] ([MaBenhNhan], [HoTen], [GioiTinh], [NamSinh], [DiaChi]) VALUES (9, N'Cao Văn Bông', 1, 1999, N'Q10')
INSERT [dbo].[BenhNhan] ([MaBenhNhan], [HoTen], [GioiTinh], [NamSinh], [DiaChi]) VALUES (10, N'Ngoc Bao Huynh', 0, 2000, N'Q7')
SET IDENTITY_INSERT [dbo].[BenhNhan] OFF
SET IDENTITY_INSERT [dbo].[ChiTietPhieuKham] ON 

INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (6, 2, 2, 1, 15000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (7, 2, 3, 2, 160000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (8, 2, 1, 3, 180000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (9, 2, 2, 4, 60000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (10, 2, 3, 5, 400000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (11, 6, 1, 2, 120000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (12, 4, 3, 1, 80000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (13, 4, 1, 4, 240000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (14, 4, 2, 3, 45000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (15, 4, 3, 3, 240000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (16, 4, 2, 3, 45000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (17, 7, 1, 2, 120000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (18, 7, 2, 2, 30000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (19, 7, 2, 2, 30000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (20, 7, 1, 2, 120000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (21, 7, 2, 1, 15000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (22, 7, 1, 2, 120000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (23, 7, 2, 2, 30000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (24, 7, 2, 2, 30000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (25, 7, 1, 2, 120000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (26, 7, 2, 1, 15000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (27, 7, 1, 2, 120000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (28, 7, 2, 2, 30000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (29, 7, 2, 2, 30000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (30, 7, 1, 2, 120000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (31, 7, 2, 1, 15000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (32, 7, 1, 2, 120000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (33, 7, 2, 2, 30000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (34, 7, 2, 2, 30000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (35, 7, 1, 2, 120000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (36, 7, 2, 1, 15000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (37, 7, 1, 2, 120000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (38, 7, 2, 2, 30000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (39, 7, 2, 2, 30000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (40, 7, 1, 2, 120000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (41, 7, 2, 1, 15000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (42, 7, 1, 2, 120000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (43, 7, 2, 2, 30000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (44, 7, 2, 2, 30000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (45, 7, 1, 2, 120000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (46, 7, 2, 1, 15000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (47, 8, 1, 1, 60000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (48, 8, 1, 1, 60000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (49, 8, 3, 1, 80000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (50, 8, 2, 1, 15000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (51, 8, 3, 1, 80000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (52, 10, 1, 1, 60000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (53, 10, 3, 1, 80000)
INSERT [dbo].[ChiTietPhieuKham] ([MaChiTietPhieuKham], [MaPhieuKham], [MaThuoc], [SoLuong], [ThanhTien]) VALUES (54, 5, 1, 1, 60000)
SET IDENTITY_INSERT [dbo].[ChiTietPhieuKham] OFF
INSERT [dbo].[HoaDonThanhToan] ([MaPhieuKham], [TienKham], [TienThuoc], [NgayLapHoaDon]) VALUES (2, 30000, 815000, CAST(N'2020-07-09T01:06:36.693' AS DateTime))
INSERT [dbo].[HoaDonThanhToan] ([MaPhieuKham], [TienKham], [TienThuoc], [NgayLapHoaDon]) VALUES (3, 30000, 0, CAST(N'2020-07-09T10:10:10.777' AS DateTime))
INSERT [dbo].[HoaDonThanhToan] ([MaPhieuKham], [TienKham], [TienThuoc], [NgayLapHoaDon]) VALUES (4, 30000, 650000, CAST(N'2020-07-16T06:49:08.223' AS DateTime))
INSERT [dbo].[HoaDonThanhToan] ([MaPhieuKham], [TienKham], [TienThuoc], [NgayLapHoaDon]) VALUES (6, 30000, 120000, CAST(N'2020-07-09T01:06:29.197' AS DateTime))
INSERT [dbo].[HoaDonThanhToan] ([MaPhieuKham], [TienKham], [TienThuoc], [NgayLapHoaDon]) VALUES (7, 30000, 1575000, CAST(N'2020-07-16T06:54:06.757' AS DateTime))
INSERT [dbo].[HoaDonThanhToan] ([MaPhieuKham], [TienKham], [TienThuoc], [NgayLapHoaDon]) VALUES (8, 30000, 295000, CAST(N'2020-07-16T15:55:43.973' AS DateTime))
INSERT [dbo].[PhieuKham] ([NgayKham], [TrieuChung], [DuDoanLoaiBenh], [MaBenhNhan]) VALUES (CAST(N'2020-07-07T00:00:00.000' AS DateTime), N'Nhức đầu', N'Nội khoa', 2)
INSERT [dbo].[PhieuKham] ([NgayKham], [TrieuChung], [DuDoanLoaiBenh], [MaBenhNhan]) VALUES (CAST(N'2020-07-07T00:00:00.000' AS DateTime), N'asdasd', N'asdasd', 3)
INSERT [dbo].[PhieuKham] ([NgayKham], [TrieuChung], [DuDoanLoaiBenh], [MaBenhNhan]) VALUES (CAST(N'2020-07-07T00:00:00.000' AS DateTime), N'Nhức đầu', N'Điên', 4)
INSERT [dbo].[PhieuKham] ([NgayKham], [TrieuChung], [DuDoanLoaiBenh], [MaBenhNhan]) VALUES (CAST(N'2020-07-08T00:00:00.000' AS DateTime), N'Ho', N'Cảm', 5)
INSERT [dbo].[PhieuKham] ([NgayKham], [TrieuChung], [DuDoanLoaiBenh], [MaBenhNhan]) VALUES (CAST(N'2020-07-08T00:00:00.000' AS DateTime), N'abc', N'def', 6)
INSERT [dbo].[PhieuKham] ([NgayKham], [TrieuChung], [DuDoanLoaiBenh], [MaBenhNhan]) VALUES (CAST(N'2020-07-18T00:00:00.000' AS DateTime), N'Nhức đầu', N'Cảm', 7)
INSERT [dbo].[PhieuKham] ([NgayKham], [TrieuChung], [DuDoanLoaiBenh], [MaBenhNhan]) VALUES (CAST(N'2020-07-18T00:00:00.000' AS DateTime), N'Ho', N'Cảm', 8)
INSERT [dbo].[PhieuKham] ([NgayKham], [TrieuChung], [DuDoanLoaiBenh], [MaBenhNhan]) VALUES (CAST(N'2020-07-16T00:00:00.000' AS DateTime), NULL, NULL, 9)
INSERT [dbo].[PhieuKham] ([NgayKham], [TrieuChung], [DuDoanLoaiBenh], [MaBenhNhan]) VALUES (CAST(N'2020-07-17T00:00:00.000' AS DateTime), N'Nhức đầu', N'Cảm', 10)
SET IDENTITY_INSERT [dbo].[Thuoc] ON 

INSERT [dbo].[Thuoc] ([MaThuoc], [TenThuoc], [DonVi], [DonGia], [CachDung]) VALUES (1, N'Natri chloride 0,45%', N'viên', 60000, N'test cách dùng 1')
INSERT [dbo].[Thuoc] ([MaThuoc], [TenThuoc], [DonVi], [DonGia], [CachDung]) VALUES (2, N'Zoledronic Acid', N'chai', 15000, N'Bôi lên vết thương hở')
INSERT [dbo].[Thuoc] ([MaThuoc], [TenThuoc], [DonVi], [DonGia], [CachDung]) VALUES (3, N'Acid Folic', N'viên', 80000, N'Uống sáng và tối mỗi thứ 1 viên')
SET IDENTITY_INSERT [dbo].[Thuoc] OFF
ALTER TABLE [dbo].[ChiTietPhieuKham]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ChiTietPhieuKham_dbo.PhieuKham_MaPhieuKham] FOREIGN KEY([MaPhieuKham])
REFERENCES [dbo].[PhieuKham] ([MaBenhNhan])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ChiTietPhieuKham] CHECK CONSTRAINT [FK_dbo.ChiTietPhieuKham_dbo.PhieuKham_MaPhieuKham]
GO
ALTER TABLE [dbo].[ChiTietPhieuKham]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ChiTietPhieuKham_dbo.Thuoc_MaThuoc] FOREIGN KEY([MaThuoc])
REFERENCES [dbo].[Thuoc] ([MaThuoc])
GO
ALTER TABLE [dbo].[ChiTietPhieuKham] CHECK CONSTRAINT [FK_dbo.ChiTietPhieuKham_dbo.Thuoc_MaThuoc]
GO
ALTER TABLE [dbo].[HoaDonThanhToan]  WITH CHECK ADD  CONSTRAINT [FK_dbo.HoaDonThanhToan_dbo.PhieuKham_MaPhieuKham] FOREIGN KEY([MaPhieuKham])
REFERENCES [dbo].[PhieuKham] ([MaBenhNhan])
GO
ALTER TABLE [dbo].[HoaDonThanhToan] CHECK CONSTRAINT [FK_dbo.HoaDonThanhToan_dbo.PhieuKham_MaPhieuKham]
GO
ALTER TABLE [dbo].[PhieuKham]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PhieuKham_dbo.BenhNhan_MaBenhNhan] FOREIGN KEY([MaBenhNhan])
REFERENCES [dbo].[BenhNhan] ([MaBenhNhan])
GO
ALTER TABLE [dbo].[PhieuKham] CHECK CONSTRAINT [FK_dbo.PhieuKham_dbo.BenhNhan_MaBenhNhan]
GO
/****** Object:  StoredProcedure [dbo].[sp_ThongKeSuDungThuoc]    Script Date: 7/17/2020 5:03:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_ThongKeSuDungThuoc]
AS
BEGIN
	SELECT DATEPART(MONTH, B.NgayKham) AS Thang, A.MaThuoc, C.TenThuoc, C.DonVi, COUNT(*) AS SoLanDung, SUM(SoLuong) As TongSoLuong
	FROM ChiTietPhieuKham A 
	OUTER APPLY (SELECT NgayKham 
				FROM PhieuKham B
				WHERE B.MaBenhNhan = A.MaPhieuKham) B
	INNER JOIN Thuoc C ON A.MaThuoc = C.MaThuoc
	GROUP BY DATEPART(MONTH, B.NgayKham), A.MaThuoc, C.TenThuoc, C.DonVi
	ORDER BY Thang
END
GO
