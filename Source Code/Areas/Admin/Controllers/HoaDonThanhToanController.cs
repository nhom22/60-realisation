﻿using System;
using System.Linq;
using System.Web.Mvc;
using Warehouse.Models;
using Warehouse.Services;

namespace Warehouse.Areas.Admin.Controllers
{
    [Authorize]
    public class HoaDonThanhToanController : Controller
    {
        QuanLyPhongMachTuEntities db = new QuanLyPhongMachTuEntities();
        PhieuKhamService _phieuKhamService = new PhieuKhamService();

        public ActionResult Index()
        {
            var hoaDonThanhToans = db.HoaDonThanhToans.OrderByDescending(x => x.NgayLapHoaDon).ToList();
            return View(hoaDonThanhToans);
        }

        public ActionResult Create(int maBenhNhan)
        {
            var phieuKham = _phieuKhamService.GetPhieuKhamCuaBenhNhan(maBenhNhan);
            HoaDonThanhToan hoaDonThanhToan = new HoaDonThanhToan()
            {
                MaPhieuKham = maBenhNhan,
                TienKham = 30000,
                TienThuoc = phieuKham.ChiTietPhieuKhams != null ? phieuKham.ChiTietPhieuKhams.Sum(x => x.ThanhTien ) ?? 0 : 0,
                NgayLapHoaDon = DateTime.Now
            };
            db.HoaDonThanhToans.Add(hoaDonThanhToan);
            db.SaveChanges();
            return RedirectToAction("Index", "PhieuKham");
        }
    }
}