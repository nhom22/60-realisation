﻿using System;
using System.Linq;
using System.Web.Mvc;
using Warehouse.Models;
using Warehouse.Services;

namespace Warehouse.Areas.Admin.Controllers
{
    [Authorize]
    public class ThuocController : Controller
    {
        ThuocService _thuocService = new ThuocService();

        #region CRUD
        public ActionResult Index()
        {
            return View(_thuocService.GetAll());
        }

        public ActionResult _CreateModal()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(Thuoc thuoc)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _thuocService.Add(thuoc);
                    return Json(new { status = 1, message = "Thêm thành công" });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return Json(new { status = 0, message = Functions.GetAllErrorsPage(ModelState) });
        }

        public ActionResult _EditModal(int id)
        {
            Thuoc thuoc = _thuocService.GetByMaThuoc(id);
            if (thuoc == null)
            {
                return Redirect("/pages/404");
            }
            return PartialView(thuoc);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(Thuoc thuoc)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _thuocService.Update(thuoc);
                    return Json(new { status = 1, message = "Sửa thành công" });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return Json(new { status = 0, message = Functions.GetAllErrorsPage(ModelState) });
        }

        public ActionResult _DeleteModal(int Id)
        {
            Thuoc thuoc = _thuocService.GetByMaThuoc(Id);
            if (thuoc == null)
            {
                return Content("<p>Dữ liệu không tồn tại trong hệ thống!</p>");
            }
            return PartialView(thuoc);
        }


        // POST: Admin/Color/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteConfirmed(int id)
        {
            try
            {
                Thuoc thuoc = _thuocService.GetByMaThuoc(id);
                if (thuoc == null)
                {
                    ModelState.AddModelError("", "Thuốc không tồn tại!");
                }
                if (ModelState.IsValid)
                {
                    _thuocService.Delete(thuoc);
                    return Json(new { status = 1 });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return Json(new { status = 0, message = Functions.GetAllErrorsPage(ModelState) });
        }
        #endregion

        [HttpPost]
        public JsonResult GetAll()
        {
            var thuocs = _thuocService.GetAll().Select(x => x.TenThuoc).ToArray();
            return Json(thuocs, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetThongTinThuoc(int maThuoc)
        {
            var thuoc = _thuocService.GetByMaThuoc(maThuoc);
            if(thuoc != null)
            {
                return Json(new { MaThuoc = thuoc.MaThuoc, TenThuoc = thuoc.TenThuoc, CachDung = thuoc.CachDung, DonVi = thuoc.DonVi, DonGia = thuoc.DonGia }, JsonRequestBehavior.AllowGet);
            }
            return Json(new Thuoc() { }, JsonRequestBehavior.AllowGet);
        }
    }
}