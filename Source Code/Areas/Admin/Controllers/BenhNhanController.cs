﻿using System.Web.Mvc;
using Warehouse.Services;

namespace Warehouse.Areas.Admin.Controllers
{
    [Authorize]
    public class BenhNhanController : Controller
    {
        BenhNhanService _benhNhanService = new BenhNhanService();

        public ActionResult Index()
        {
            var benhNhans = _benhNhanService.GetDanhSachBenhNhan();
            return View(benhNhans);
        }
    }
}