﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Models;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;

namespace Warehouse.Areas.Admin.Controllers
{
    [Authorize]
    public class AspNetUserController : Controller
    {

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
       
        public AspNetUserController()
        {

        }

        public AspNetUserController(ApplicationSignInManager signInManager, ApplicationUserManager userManager)
        {
            SignInManager = signInManager;
            UserManager = userManager;
        }


        #region Public Property 
        public string UserId
        {
            get
            {
                return User.Identity.GetUserId();
            }
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        #endregion

        public PartialViewResult _UserLoggedPartial()
        {
            ApplicationUser applicationUser = UserManager.FindById(UserId);
            return PartialView(applicationUser);
        }

        public PartialViewResult _UserPanelPartial()
        {
            ApplicationUser applicationUser = UserManager.FindById(UserId);
            return PartialView(applicationUser);
        }

        public ViewResult ProfileUser()
        {
            ApplicationUser model = UserManager.FindById(UserId);
            ViewBag.Title = "Hồ sơ cá nhân";
            UpdateInfoViewModel updateInfoViewModel = new UpdateInfoViewModel()
            {
                Address = model.Address,
                UserName = model.UserName,
                FullName = model.FullName,
                Id = model.Id,
                PhoneNumber = model.PhoneNumber,
                RoleId = ViewBag.RoleId,
                Avatar = model.Avatar,
                Email = model.Email
            };
            return View(updateInfoViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProfileUser([Bind(Exclude = "UserName")] UpdateInfoViewModel updateInfoViewModel)
        {
            var model = UserManager.FindById(UserId);
            if (ModelState.IsValid)
            {
                model.FullName = updateInfoViewModel.FullName;
                model.Address = updateInfoViewModel.Address;
                model.Email = updateInfoViewModel.Email;
                model.PhoneNumber = updateInfoViewModel.PhoneNumber;
                UserManager.Update(model);
                return RedirectToAction("ProfileUser", new { Id = updateInfoViewModel.Id });
            }
            ViewBag.Title = "Hồ sơ cá nhân";
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeAvatar(string userId, string base64String)
        {
            if (User.IsInRole("Admin") == false)
            {
                userId = this.UserId;
            }
            if (!string.IsNullOrEmpty(base64String))
            {
                var model = UserManager.FindById(userId);
                try
                {
                    base64String = base64String.Substring(base64String.IndexOf(',') + 1);
                    string newAvatar = model.UserName + DateTime.Now.Ticks.ToString() + ".jpg";
                    Functions.SaveFileFromBase64(Server.MapPath("~/Photos/Users/" + newAvatar), base64String);
                    model.Avatar = newAvatar;
                }
                catch (Exception ex)
                {
                    return RedirectToAction("ProfileUser", new { Id = userId });
                }
                UserManager.Update(model);
            }

            return RedirectToAction("ProfileUser", new { Id = userId });
        }

        [AllowAnonymous]
        public ActionResult Employees()
        {
            List<ApplicationUser> applicationUsers = UserManager.Users.ToList();
            return View(applicationUsers);
        }

        [Authorize(Users = "admin")]
        public ActionResult _EditEmployeeModal(string Id)
        {
            ApplicationUser applicationUser = UserManager.FindById(Id);
            if (applicationUser == null)
            {
                return Content("<p>Dữ liệu không tồn tại trong hệ thống!</p>");
            }
            return PartialView(applicationUser);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "admin")]
        public JsonResult EditEmployee([Bind(Include = "Id,FullName,PhoneNumber,Address")] ApplicationUser applicationUser)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = UserManager.FindById(applicationUser.Id);
                try
                {
                    user.FullName = applicationUser.FullName;
                    user.PhoneNumber = applicationUser.PhoneNumber;
                    user.Address = applicationUser.Address;
                    UserManager.Update(user);
                    return Json(new { status = 1, message = "Update success." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            return Json(new { status = 0, message = Functions.GetAllErrorsPage(this.ModelState) });
        }


        [Authorize(Users = "admin")]
        [AllowAnonymous]
        /// <summary>
        /// Thêm Nhân Viên
        /// </summary>
        /// <returns></returns>
        public ActionResult _CreateModal()
        {
            return PartialView(new CreateUserViewModel());
        }

        //[Authorize(Users = "admin")]
        /// <summary>
        /// Thêm Nhân viên
        /// </summary>
        /// <param name="createUserViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<JsonResult> Create(CreateUserViewModel createUserViewModel)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser()
                {
                    UserName = createUserViewModel.UserName,
                    Email = createUserViewModel.Email,
                    FullName = createUserViewModel.FullName,
                    Avatar = "user.png",
                    DateRegister = DateTime.Now,
                    EmailConfirmed = true
                };
                try
                {
                    IdentityResult result = await UserManager.CreateAsync(user, createUserViewModel.Password);
                    if (result.Succeeded)
                    {
                        return Json(new { status = 1, message = "Thêm thành công" });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            return Json(new { status = 0, message = Functions.GetAllErrorsPage(ModelState) });
        }

        [Authorize(Users = "admin")]
        public ActionResult _DeleteModal(string Id)
        {
            ApplicationUser applicationUser = UserManager.FindById(Id);
            if (applicationUser == null)
            {
                return Content("<p>Dữ liệu không tồn tại trong hệ thống!</p>");
            }
            return PartialView(applicationUser);
        }

        [Authorize(Users = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Delete(string Id)
        {
            try
            {
                ApplicationUser user = await UserManager.FindByIdAsync(Id);
                await UserManager.DeleteAsync(user);
                return Json(new { status = 1, message = "Update success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return Json(new { status = 0, message = Functions.GetAllErrorsPage(this.ModelState) }, JsonRequestBehavior.AllowGet);
        }


        public ViewResult Lock(string Id)
        {
            ApplicationUser aspNetUser = UserManager.FindById(Id);
            return View(aspNetUser);
        }


        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ChangePassword(ChangePasswordViewModel changePasswordViewModel)
        {
            if (ModelState.IsValid)
            {
                var result = UserManager.ChangePassword(UserId, changePasswordViewModel.OldPassword, changePasswordViewModel.NewPassword);
                if (result.Succeeded)
                {
                    var user = UserManager.FindById(UserId);
                    if (user != null)
                    {
                        SignInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                    }
                    return Json(new { status = 1, message = "Đổi mật khẩu thành công." }, JsonRequestBehavior.AllowGet);
                }
                AddErrors(result);
            }  
            return Json(new { status = 0, message = Functions.GetAllErrorsPage(ModelState)}, JsonRequestBehavior.AllowGet);
        }


        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}
