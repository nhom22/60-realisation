﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Warehouse.Models;

namespace Warehouse.Areas.Admin.Controllers
{
    [Authorize]
    public class BaoCaoController : Controller
    {
        QuanLyPhongMachTuEntities db = new QuanLyPhongMachTuEntities();

       
        public ActionResult DoanhThuTheoNgay()
        {
            List<f_ThongKeDoanhThuTheoNgay_Result> doanhThuTheoNgay = db.f_ThongKeDoanhThuTheoNgay().ToList();
            Dictionary<int, List<f_ThongKeDoanhThuTheoNgay_Result>> NhomDoanhThuTheoNgay = doanhThuTheoNgay.GroupBy(x => x.NgayKham.Month)
                .Select(x => new { Thang = x.Key, DanhSachChiTiet = x }).OrderBy(x => x.Thang).ToDictionary(x => x.Thang, x => x.DanhSachChiTiet.ToList());
            return View(NhomDoanhThuTheoNgay);
        }

        //public ActionResult SuDungThuoc()
        //{
        //    ViewBag.Thang = db.PhieuKhams.Select(x => x.NgayKham.Month).OrderBy(x => x).ToList();
        //    var thongKeDungThuoc = db.ChiTietPhieuKhams.GroupBy(x => new { Thang = x.PhieuKham.NgayKham.Month, Thuoc = x.Thuoc })
        //                        .Select(x => new { Thang = x.Key.Thang, TenThuoc = x.Key.Thuoc.TenThuoc, DonVi = x.Key.Thuoc.DonVi, SoLuong = x.Sum(y => y.SoLuong) ?? 0, SoLanDung = x.Count() })
        //                        .GroupBy(x => x.Thang)
        //                        .ToDictionary(x => x.Key, x => x.Select(y => new BaoCaoSuDungThuoc() { TenThuoc = y.TenThuoc, DonVi = y.DonVi, SoLuong = y.SoLuong, SoLanDung = y.SoLanDung }));
        //    return View(thongKeDungThuoc);
        //}

    
    }
}