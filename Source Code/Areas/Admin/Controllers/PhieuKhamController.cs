﻿using System.Collections.Generic;
using System.Web.Mvc;
using Warehouse.Models;
using Warehouse.Services;

namespace Warehouse.Areas.Admin.Controllers
{
    [Authorize]
    public class PhieuKhamController : Controller
    {
        PhieuKhamService _phieuKhamService = new PhieuKhamService();
        ChiTietPhieuKhamService _chiTietPhieuKhamService = new ChiTietPhieuKhamService();
        BenhNhanService _benhNhanService = new BenhNhanService();
        ThuocService _thuocService = new ThuocService();

        public ActionResult Index()
        {
            var phieuKhams = _phieuKhamService.GetAll();
            return View(phieuKhams);
        }

        public ActionResult _DetailModal(int maBenhNhan)
        {
            var phieuKham = _phieuKhamService.GetPhieuKhamCuaBenhNhan(maBenhNhan);
            if(phieuKham == null)
            {
                return Content("Không có dữ liệu!");
            }
            ViewBag.ChiTietPhieuKhams = _chiTietPhieuKhamService.GetByMaPhieuKham(phieuKham.MaBenhNhan);
            return PartialView(phieuKham);
        }

        public PartialViewResult _CreateModal(int maBenhNhan)
        {
            ViewBag.BenhNhan = _benhNhanService.GetByMaBenhNhan(maBenhNhan);
            ViewBag.Thuocs = _thuocService.GetAll();
            return PartialView();
        }

        [HttpPost]
        public JsonResult _CreateModal(int maBenhNhan, FormCollection formCollection)
        {
            PhieuKham phieuKham = _phieuKhamService.GetPhieuKhamCuaBenhNhan(maBenhNhan);
            if(phieuKham != null)
            {
                phieuKham.TrieuChung = formCollection["TrieuChung"];
                phieuKham.DuDoanLoaiBenh = formCollection["DuDoanLoaiBenh"];
            }
            List<ChiTietPhieuKham> chiTietPhieuKhams = new List<ChiTietPhieuKham>();
            for(int i = 1; i <= 5; i++)
            {
                if (formCollection["Thuoc_" + i] != null && formCollection["Thuoc_" + i].ToString() != "")
                {
                    Thuoc thuoc = _thuocService.GetByMaThuoc(int.Parse(formCollection["Thuoc_" + i].ToString()));
                    ChiTietPhieuKham chiTietPhieuKhamMoi = new ChiTietPhieuKham()
                    {
                        MaPhieuKham = phieuKham.MaBenhNhan,
                        MaThuoc = thuoc.MaThuoc,
                        SoLuong = formCollection["SoLuong_" + i] != null ? (byte?)byte.Parse(formCollection["SoLuong_" + i].ToString()) : null,
                    };
                    chiTietPhieuKhamMoi.ThanhTien = thuoc.DonGia == null ? null : (int?)(chiTietPhieuKhamMoi.SoLuong * thuoc.DonGia.Value);
                    chiTietPhieuKhams.Add(chiTietPhieuKhamMoi);
                }
            }
            if(ModelState.IsValid)
            {
                _phieuKhamService.Update(phieuKham);
                _chiTietPhieuKhamService.AddList(chiTietPhieuKhams);
                return Json(new { status = 1 });
            }
            return Json(new { status = 0, message = Warehouse.Models.Functions.GetAllErrorsPage(this.ModelState) });
        }

    }
}