﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Warehouse.Models;
using Warehouse.Services;

namespace Warehouse.Areas.Admin.Controllers
{
    [Authorize]
    public class KhamBenhController : Controller
    {
        BenhNhanService _benhNhanService = new BenhNhanService();
        PhieuKhamService _phieuKhamnService = new PhieuKhamService();

        public ActionResult Index()
        {
            var khamBenhs = _benhNhanService.GetDanhSachKhamBenh();
            return View(khamBenhs);
        }

        public PartialViewResult _CreateModal()
        {
            return PartialView();
        }

        [HttpPost]
        public JsonResult _CreateModal(FormCollection formCollection)
        {  
            DateTime ngayKham;
            if(DateTime.TryParseExact(formCollection["NgayKham"].ToString(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out ngayKham) == false)
            {
                ModelState.AddModelError("NgayKham", "Ngày khám phải là ngày hợp lệ với định dạng dd/MM/yyyy (ví dụ 05/12/2020)!");
                return Json(new { status = 0, message = Warehouse.Models.Functions.GetAllErrorsPage(this.ModelState) }, JsonRequestBehavior.AllowGet);
            }
            List<BenhNhan> benhNhans = new List<BenhNhan>();
            for(int i = 1; i <= 5; i++)
            {
                if (formCollection["HoTen_" + i] != null && formCollection["HoTen_" + i].ToString() != "")
                {
                    BenhNhan benhNhanMoi = new BenhNhan()
                    {
                        HoTen = formCollection["HoTen_" + i].ToString(),
                        GioiTinh = formCollection["GioiTinh_" + i] != null ? (int.Parse(formCollection["GioiTinh_" + i].ToString()) == 1 ? true : false) : false,
                        NamSinh = formCollection["NamSinh_" + i] != null ? (int?)int.Parse(formCollection["NamSinh_" + i].ToString()) : null,
                        DiaChi = formCollection["DiaChi_" + i] 
                    };
                    if(_benhNhanService.KiemTraLuotKhamQua40Nguoi(ngayKham))
                    {
                        ModelState.AddModelError("", "Lượng khám trong ngày này đã vượt đủ 40 người. Không thể tạo thêm!");
                        break;
                    }

                    if (ModelState.IsValid)
                    {
                        _benhNhanService.Add(benhNhanMoi);
                        PhieuKham phieuKhamMoi = new PhieuKham()
                        {
                            MaBenhNhan = benhNhanMoi.MaBenhNhan,
                            NgayKham = ngayKham
                        };
                        _phieuKhamnService.Add(phieuKhamMoi);
                    }
                }
            }
            if(ModelState.IsValid)
            {
                return Json(new { status = 1 }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = 0, message = Warehouse.Models.Functions.GetAllErrorsPage(this.ModelState) }, JsonRequestBehavior.AllowGet);
        }
    }
}